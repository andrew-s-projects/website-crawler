/*********************************************************
	Description: Contains the main function used to run the URL parser and interface with a website
*********************************************************/

#pragma once
#include "pch.h"
#include "crawler.h"
#include "winsock.h"
#include "common.h"
#include "urlparser.h"
#include "access.h"
#define MAX_TIME 10000
Accessories* access=new Accessories();
int main(int argc, char* argv[])
 {
	if (argc < 2) {
		std::cout << "Incorrect number of arguments. Correct use is #of Threads filename";
		return 0;
	}
	
	Winsock ws;
	Winsock::initialize();	// initialize 
	URLParser parser;
	int count = 1; //used to show current URL number from file
	int cnt = 0;
	int THREAD_COUNT = stoi(argv[1]);
	string file = argv[2];
	
	//URLParser parser(url);
	std::queue<std::string>urls;
	std::unordered_set<string>hosts;
	std::unordered_set<DWORD>ips;
	
	std::string line;
	std::string code = "";
	ifstream myfile(file);
	if (myfile.is_open())
	{

		while (getline(myfile, line)) {
			
			urls.push(line);

		}
		std::cout << "Opened " << file << " with size " << access->getFileSize(file) << '\n';
		myfile.close();
	}

// thread handles are stored here; they can be used to check status of threads, or kill them
HANDLE *ptrs = new HANDLE[THREAD_COUNT];
Parameters p;

int num_peers = 20;

 //create a mutex for accessing critical sections (including printf)
p.mutex = CreateMutex(NULL, 0, NULL);

 //create a semaphore that check if a thread finishs its task
p.finished = CreateSemaphore(NULL, 0, THREAD_COUNT, NULL);

p.num_tasks = THREAD_COUNT;
p.sharedQ = urls;
p.sharedQSize = urls.size();
p.active_threads = 0;
p.sharedCount.push_back(urls.size());
p.thehosts = hosts;
p.theips = ips;


 //create a manual reset event to determine the termination condition is true
p.eventQuit = CreateEvent(NULL, true, false, NULL);

 //create a semaphore to keep track of the number of items in the inputQ. The initial size of inputQ is num_peers
p.semaQ = CreateSemaphore(NULL, num_peers, MAX_SEM_COUNT , NULL);

 //get current system time
DWORD t = timeGetTime();

for (int i = 0; i < THREAD_COUNT; ++i)
{
	 //structure p is the shared space between the threads	
	ptrs[i] = CreateThread(NULL, 4096, (LPTHREAD_START_ROUTINE)thread, &p, 0, NULL);
	if (i == 0) {
		DWORD first_thread_id = GetThreadId(ptrs[i]);
		p.print_thread_id = first_thread_id;
		
	}
	
}

printf("-----------created %d threads-----------\n", THREAD_COUNT);


for (int i = 1; i <= THREAD_COUNT; ++i)
{
	WaitForSingleObject(p.finished, INFINITE);
	printf("%d thread finished. main() function there--------------\n", i);
}
int total=0;
//don't need this
for (std::vector<int>::const_iterator i = p.extracted.begin(); i != p.extracted.end(); i++) {
	char temp = *i;
	total += temp;
	int average = total / p.time;
}
int totallength = 0;
for (std::vector<int>::const_iterator i = p.totalBytes.begin(); i != p.totalBytes.end(); i++) {
	char temp = *i;
	totallength += temp;
}
int average = total / p.time;
printf("Extracted  %d URLS @%d/s\n", total, average);
int robotAverage = p.robotSuccess.size() / p.time;
int dnsAverage = p.dnsSuccess.size() / p.time;
printf("Looked up %d DNS names @%d/s\n", p.dnsSuccess.size(), dnsAverage);
printf("Downloaded %d robots @%d /s\n", p.robotSuccess.size(), robotAverage);
int crawledAverage = p.goodCode.size() / p.time;
printf("Crawled %d pages @ %d/s (%d) bytes\n", p.goodCode.size(), crawledAverage);
printf("Parsed %d links\n", p.totalLinks.size());
int twoxx=0, threexx=0, fourxx=0, fivexx=0;
for (std::vector<std::string>::const_iterator i = p.thecodes.begin(); i != p.thecodes.end(); i++) {
	std::string temp = *i;
	char test = temp[0];
	if (test == '2') {
		twoxx++;
	}
	if (test == '3') {
		threexx++;
	}
	if (test == '4') {
		fourxx++;
	}
	if (test == '5') {
		fivexx++;
	}
}

printf("HTTP codes: 2xx = %d, 3xx = %d, 4xx = %d, 5xx = %d ", twoxx, threexx, fourxx, fivexx);

printf("Terminating main(), completion time %d ms\n", timeGetTime() - t);


Winsock::cleanUp();

printf("Enter any key to continue ...\n");
getchar();
return 0;
}

