#pragma once
#pragma once

#include "common.h"
#include "winsock.h"
#include "urlparser.h"
#include "access.h"



// this class is passed to all threads, acts as shared memory
class Parameters {
public:
	HANDLE mutex;
	HANDLE finished;
	HANDLE eventQuit;
	HANDLE semaQ;
	int num_tasks;
	int active_threads;
	int time = 2;
	int sharedQSize;
	DWORD print_thread_id;
	std::vector<int>sharedCount;
	std::vector<int>dnsSuccess;
	std::vector<int>robotSuccess;
	std::vector<int>goodCode;
	std::vector<int>totalBytes;
	std::vector<int>totalLinks;
	std::vector<std::string>thecodes;
	std::vector<int>extracted; //number of extracted URLs at given time
	std::queue<std::string>sharedQ;
	std::unordered_set<string>thehosts;
	std::unordered_set<DWORD>theips;
	URLParser parser;

};

// this function is where the thread starts
static UINT thread(LPVOID pParam)
{
	Parameters *p = ((Parameters*)pParam);

	Winsock ws;
	Accessories access;
	// wait for mutex, then print and sleep inside the critical section
	WaitForSingleObject(p->mutex, INFINITE);					// lock mutex
	ReleaseMutex(p->mutex);										
	std::vector<std::string>url;
	p->active_threads++;

	HANDLE	arr[] = { p->eventQuit, p->semaQ };
	while (true)
	{
		if (WaitForMultipleObjects(2, arr, false, INFINITE) == WAIT_OBJECT_0) // the eventQuit has been signaled 
			break;
		if (GetCurrentThreadId() == p->print_thread_id) {
			while (true) {
				if (p->sharedQ.size() <= 0) {
					break;
				}
				
				Sleep(2000);
				printf("[  %d]     %d  Q     %d  E      %d  H     %d  D     %d  I     %d  R     %d  C     %d  L\n", p->time, p->active_threads, p->sharedQ.size(), p->thehosts.size(), p->dnsSuccess.size(), p->theips.size(), p->robotSuccess.size(), p->goodCode.size(), p->totalLinks.size());
				p->time += 2;
			}

		}
		else // semaQ is signaled. decreased the semaphore count by 1
		{
			while (true) {
					// obtain ownership of the mutex
					WaitForSingleObject(p->mutex, INFINITE);
					if (p->sharedCount.front() < 1) {
						ReleaseMutex(p->mutex);
						break;
					}

					// ------------- entered the critical section ------------------
					url.push_back(p->sharedQ.front());

					p->sharedCount.front() = p->sharedCount.front() - 1;
					p->sharedQ.pop();
					p->extracted.push_back(1);
					p->parser = url.front();
					std::string host = p->parser.getHost();
					std::string path = p->parser.getPath();
					std::string query = p->parser.getQuery();
					DWORD ip = ws.getIPaddress(host);
				
					p->thehosts.insert(host);
					p->theips.insert(ip);
					int currentHostSize = p->thehosts.size();
					int currentIPSize = p->theips.size();
					std::string hostUnique = access.unique(currentHostSize, p->thehosts);
					std::string ipsUnique = access.ipsUnique(currentIPSize, p->theips);
					//if DNS lookup fails,  INADDR_NONE will return (1)

					if (ip != -1) {
						//add to number of successful DNS lookups
						p->dnsSuccess.push_back(1);
					}
					//Sleep(1000);
					url.pop_back();
					ReleaseMutex(p->mutex);
					//=========Do Robot Check ===========

					std::string robotRequest = p->parser.getRequest(host, "robots.txt", "", "HEAD");
					std::string robotResponse = ws.crawl(host, 80, robotRequest);
					if (robotResponse.length() != 0) {
						int count = 0;
						std::string::size_type n;
						n = robotResponse.find("200");
						std::string code = robotResponse.substr(9, 3);
						std::vector<char> test(code.c_str(), code.c_str() + code.size() + 1);

						for (std::vector<char>::const_iterator i = test.begin(); i != test.end(); i++) {
							char temp = *i;
							if (isdigit(temp)) {
								count++;
							}
						}
						if (count != 3) {

						}
						count = 0;
						if (n == std::string::npos) {
							//robot passed
							WaitForSingleObject(p->mutex, INFINITE);
							p->robotSuccess.push_back(1);
							ReleaseMutex(p->mutex);
							std::string get = p->parser.getRequest(host, path, query, "GET");
							std::string response = ws.crawl(host, 80, get);
							//std::cout << response << '\n';
							std::string thecode;
							if (response.length() != 0)
							{
								std::string::size_type n;
								std::string::size_type m;
								n = response.find("a href");
								m = response.find("A HREF");
								if (n != std::string::npos || m!=std::string::npos) {
									p->totalLinks.push_back(1);
								}
								p->totalBytes.push_back(response.length());
								thecode = response.substr(9, 3);

							}
							char car = thecode[0];
							if (car != '4' & (car == '1' || car == '2' || car == '3' || car == '5')) {
								p->goodCode.push_back(1);
							}

							std::vector<char> testcode(thecode.c_str(), thecode.c_str() + thecode.size() + 1);

							for (std::vector<char>::const_iterator i = test.begin(); i != test.end(); i++) {
								char temp = *i;
								if (isdigit(temp)) {
									count++;
								}
							}
							//all three digits are a number, "valid" code
							if (count == 3) {
								WaitForSingleObject(p->mutex, INFINITE);
								p->thecodes.push_back(thecode);
								ReleaseMutex(p->mutex);
							}
							count = 0;
						}

					}

				}


				WaitForSingleObject(p->mutex, INFINITE);

				p->num_tasks--;

				if (p->num_tasks == 0)
					SetEvent(p->eventQuit);

				ReleaseMutex(p->mutex); 
													
			}
		}
	// end of while loop for this thread
		p->active_threads--;
		// signal that this thread is exiting 
	ReleaseSemaphore(p->finished, 1, NULL);

	return 0;
}
