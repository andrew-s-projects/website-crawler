/*********************************************************
	Description: Contains the defintions of functions used to parse a URL
*********************************************************/
#pragma once
#pragma once
#include "common.h"

class URLParser {

public:

	// constructor 
	URLParser(std::string & link)  // & means pass the address of link to this function
	{
		url = link;
		host = "";
		port = 80;  // default port is 80 for web server
		path = "/";  // if path is empty, use "/"
		query = "";
		index = 0;
	}
	URLParser()=default;


	/* url format:
	* scheme://[user:pass@]host[:port][/path][?query][#fragment]
	*/

	// e.g., url: "http://cs.somepage.edu:467/index.php?addrbook.php"
	// host: "cs.somepage.edu"
	std::string getHost()
	{

		char nextChar = ':';
		char nextChar2 = '/';
		char nextChar3 = '?';
		char nextChar4 = '#';
		for (char const &c : url) {
			if (c == '/') {
				index++;
			}
			if (c == nextChar  &&index>0) {
				break;
			}
			if (c == nextChar2 && index > 2) {
				break;
			}
			if (c == nextChar3 && index > 0) {
				break;
			}
			if (c == nextChar4 && index > 0) {
				break;
			}
			if (index > 1 && c != nextChar &&c!=nextChar2) {
				host += c;
			}
		}

		// implement here, you may use url.find(...) 
		
		index = 0;
		return host;
	}

	// e.g., url: "http://cs.somepage.edu:467/index.php?addrbook.php"
	// port: 467
	short getPort()
	{
		std::string sPort = "";

		// implement here: find substring that represents the port number

		if (sPort.length() > 0)
			port = atoi(sPort.c_str());  // convert substring sPort to an integer value

		return port;
	}

	// url: "http://cs.somepage.edu:467/index.php?addrbook.php"
	// path is "/index.php"
	std::string getPath()
	{
		index = 0;
		// implement here
		char nextChar = '#';
		char nextChar2 = '?';
		char nextChar3 = '/';
		std::string temp = "";
		for (char const &c : url) {
			if (c == '/') {
				index++;
			}
			if (c == nextChar || c == nextChar2) {
				break;
			}
			if (index >= 3 && c!=nextChar && c!=nextChar2) {
				if(c=='/'){}
				else {
					temp += c;
				}
			}
		}
		//index = 0;
		path =temp;
		return path;
	}

	// url: "http://cs.somepage.edu:467/index.php?addrbook.php"
	// query is "?addrbook.php"
	std::string getQuery()
	{
		index = 0;

		char nextChar = '#';
		for (char const &c : url) {
			if (c == '?') {
				index++;
			}
			if (c == nextChar) {
				break;
			}
			if (index > 0 && c!=nextChar) {
				query+=c;
			}
		}
		// implement here
		//index = 0;
		return query;
	}
	std::string getRequest(std::string host, std::string path, std::string query,std::string type) {
		std::string request = type+ "  /" + path + query + " HTTP/1.0\nUser-agent:  UDCScrawler/1.0\nHost: " + host + "\nConnection: close" + "\r\n\r\n";
		std::cout << request;
		return request;
	}
	


private:
	int index;
	std::string url;
	std::string host;
	short port;
	std::string path;
	std::string query;

};