/*********************************************************
	Description: Contains the defintions of functions used to interface with websites
*********************************************************/

#pragma once
#pragma once
#include "common.h" 
#include "urlparser.h"
#include "access.h"
#define DEFAULT_LENGTH 512
#define MAX_RECEIVE 9600
// the .h file defines all windows socket functions 
using std::chrono::system_clock;
class Winsock
{

	Accessories * access = new Accessories();
public:

	static void initialize()   // call Winsock::intialize() in main, to intialize winsock only once
	{
		WSADATA wsaData;
		int iResult;

		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);   // defined in winsock2.h

		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
			WSACleanup();
			exit(1); // quit program
		}
	}


	static void cleanUp() // call Winsock::cleanUp() in main only once
	{
		WSACleanup();
	}

	int createTCPSocket(void)
	{
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock == INVALID_SOCKET) {
			//printf("socket() error %d\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}
		return 0;
	}

	// host (e.g., "www.google.com" or "132.145.2.1"), return its corresponding IP address of type DWORD
	DWORD getIPaddress(string host)
	{
		struct sockaddr_in server; // structure for connecting to server
		struct hostent *remote;    // structure used in DNS lookups

		// first assume that the string is an IP address
		DWORD IP = inet_addr(host.c_str());
		if (IP == INADDR_NONE)
		{
			if ((remote = gethostbyname(host.c_str())) == NULL)
			{
				//printf("Invalid host name string: not FQDN\n");
				return INADDR_NONE;  // 1 means failed
			}
			else // take the first IP address and copy into sin_addr
			{
				memcpy((char *)&(server.sin_addr), remote->h_addr, remote->h_length);
				IP = server.sin_addr.S_un.S_addr;

			}
		}
		
		return IP;
	}

	// host IP address in binary version, port: 2-byte short
	int connectToServerIP(DWORD IP, short port)
	{
		if (IP == INADDR_NONE)
		{
			//printf("Invalid IP address\n");
			return 1;  // 1 means error
		}
		struct sockaddr_in server; // structure for connecting to server
		server.sin_addr.S_un.S_addr = IP; // irectly drop its binary version into sin_addr

		// setup the port # and protocol type
		server.sin_family = AF_INET;   // IPv4
		server.sin_port = htons(port); // host-to-network flips the byte order
		auto start = system_clock::now();
		if (connect(sock, (struct sockaddr*) &server, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
		{
			//auto start = system_clock::now();
			//printf("Connection error: %d\n", WSAGetLastError());
			return 1;
		}
		auto stop = system_clock::now();
		double time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
		//printf("Connectv done in %.0f ms found %s on port %d\n", time, inet_ntoa(server.sin_addr), htons(server.sin_port));



		return 0;
	}

	// define your sendRequest(...) function, to send a HEAD or GET request
	bool sendRequest(std::string &request) {
		if (send(sock, request.c_str(), strlen(request.c_str()), 0) == SOCKET_ERROR)
		{
			//printf("send () error - %d\n", WSAGetLastError());
			return false;
		}
		//printf("Connection on page\n"); 
		int iResult = shutdown(sock, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			wprintf(L"shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(sock);
			WSACleanup();
			return 1;
		}
		return true;
	}
	string  receive() {
		char recbuff[DEFAULT_LENGTH];
		int totalBytes = 0;
		memset(recbuff, '\0', DEFAULT_LENGTH);
		//keep receiving until there are 0 bytes in buffer
		auto start = system_clock::now();
		do {
			result = recv(sock, recbuff, DEFAULT_LENGTH - 1, 0);
			if (result > 0) {
				if (result > MAX_RECEIVE) {
					break;
				}
				
			}
			else if (result == 0) {
				
			}
			else {
				
			}

		} while (result > 0);

		auto stop = system_clock::now();
		double time = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();

		string str = recbuff;
		return str;
	}
	string crawl(std::string host, short port, std::string request) {

		createTCPSocket();
		DWORD ip = getIPaddress(host);
		connectToServerIP(ip, port);
		char temp[DEFAULT_LENGTH];
		//memset(temp, '\0', DEFAULT_LENGTH);
		std::string receiveBuff = "";
		//only check ip for robot request

		if (sendRequest(request)) {

			receiveBuff = receive();
			
			closeSocket();
		}
		
		return receiveBuff;


	}
	void closeSocket(void)
	{
		closesocket(sock);
	}


private:
	SOCKET sock;
	string message = "";
	int result;


};